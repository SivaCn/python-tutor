"""
From: Pinedo, Ruben A [mailto:rapinedo@miners.utep.edu]
Sent: Wednesday, October 16, 2013 2:49 PM
To: tutor@python.org
Subject: [Tutor] Help please

I was given this code and I need to modify it so that it will:

#1. Error handling for the files to ensure reading only .txt file
#2. Print a range of top words... ex: print top 10-20 words
#3. Print only the words with > 3 characters
#4. Modify the printing function to print top 1 or 2 or 3 ....
#5. How many unique words are there in the book of length 1, 2, 3 etc

I am fairly new to python and am completely lost, i looked in my book as to how to do number one but i cannot figure out what to modify and/or delete to add the print selection. This is the code:


import string

def process_file(filename):
    hist = dict()
    fp = open(filename)
    for line in fp:
        process_line(line, hist)
    return hist

def process_line(line, hist):
    line = line.replace('-', ' ')

    for word in line.split():
        word = word.strip(string.punctuation + string.whitespace)
        word = word.lower()

        hist[word] = hist.get(word, 0) + 1

def common_words(hist):
    t = []
    for key, value in hist.items():
        t.append((value, key))

    t.sort(reverse=True)
    return t

def most_common_words(hist, num=100):
    t = common_words(hist)
    print 'The most common words are:'
    for freq, word in t[:num]:
        print freq, '\t', word

hist = process_file('emma.txt')
print 'Total num of Words:', sum(hist.values())
print 'Total num of Unique Words:', len(hist)
most_common_words(hist, 50)

Any help would be greatly appreciated because i am struggling in this class. Thank you in advance

Respectfully,

Ruben Pinedo
Computer Information Systems
College of Business Administration
University of Texas at El Paso
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/tutor/attachments/20131017/ea525e7b/attachment.html>

"""
import operator

from string import whitespace as space
from string import punctuation as punc

class TextProcessing(object):
    """."""
    def __init__(self):
        """."""
        self.file = None
        self.sorted_list = []
        self.words_and_occurence = {}

    def __sort_dict_by_value(self):
        """."""
        sorted_in_rev = sorted(self.words_and_occurence.items(), key=lambda x: x[1])
        self.sorted_list = sorted_in_rev[::-1]

    def __validate_words(self, word):
        """."""
        if word in self.words_and_occurence:
            self.words_and_occurence[word] += 1
        else:
            self.words_and_occurence[word] = 1

    def __parse_file(self, file_name):
        """."""
        fp = open(file_name, 'r')
        line = fp.readline()
        while line:
            split_line = [self.__validate_words(word.strip(punc + space)) \
                          for word in line.split()
                          if word.strip(punc + space)]
            
            line = fp.readline()
        fp.close()

    def parse_file(self, file_name=None):
        """."""
        if file_name is None:
            raise Exception("Please pass the file to be parsed")
        if not file_name.endswith(r".txt"):
            raise Exception("*** Error *** Not a valid text file")

        self.__parse_file(file_name)
        
        self.__sort_dict_by_value()

    def print_top_n(self, n):
        """."""
        print "Top {0} words:".format(n), [self.sorted_list[i][0] for i in xrange(n)]

    def print_unique_words(self):
        """."""
        print "Unique words:", [self.sorted_list[i][0] for i in xrange(len(self.sorted_list))]

if __name__ == "__main__":
    """."""
    obj = TextProcessing()
    obj.parse_file(r'test_input.txt')
    obj.print_top_n(4)
    obj.print_unique_words()
